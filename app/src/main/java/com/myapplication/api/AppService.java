package com.myapplication.api;

import com.myapplication.model.Backdrop;
import com.myapplication.model.BackdropData;
import com.myapplication.model.Example;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AppService {
    @FormUrlEncoded
    @POST("3/movie/upcoming")
    Call<Example> getMovies(@FieldMap Map<String, String> params);

    @GET("3/movie/{id}/images?api_key=b7cd3340a794e5a2f35e3abb820b497f")
    Call<BackdropData> groupList(@Path("id") int groupId);
}
