package com.myapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.myapplication.api.ApiRequestHelper;
import com.myapplication.model.Backdrop;
import com.myapplication.model.BackdropData;
import com.myapplication.model.Result;
import com.myapplication.util.ConnectionDetector;
import com.myapplication.util.Utils;
import com.myapplication.widget.CustomTextViewRegular;
import com.myapplication.widget.materialprogress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MXCPUU11 on 9/11/2017.
 */

public class DetailActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    ArrayList<String> slider_image_list;
    @BindView(R.id.custom_indicator)
    PagerIndicator customIndicator;
    @BindView(R.id.tv_title)
    CustomTextViewRegular tvTitle;
    @BindView(R.id.tv_overview)
    CustomTextViewRegular tvOverview;
    @BindView(R.id.ratingBar1)
    RatingBar ratingBar1;
    @BindView(R.id.slider)
    SliderLayout mDemoSlider;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular textViewTitle;
    private TextView[] dots;
    int page_position = 0;
    Context context;
    HashMap<String, String> file_maps;
    List<String> listoverview = new ArrayList<String>();
    MyApp app;
    ConnectionDetector connectionDetector;
    List<Backdrop> posters = new ArrayList<Backdrop>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);
        app = (MyApp) getApplication();
        context = DetailActivity.this;
        connectionDetector = new ConnectionDetector(context);
        init();

    }

    private void init() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Intent intent = getIntent();
        final Result data = (Result) intent.getSerializableExtra("id");
        int id = data.getId();
        final String overview = data.getOverview();
        final String title = data.getTitle();
        textViewTitle.setText(data.getTitle());
        final float d = (float) ((data.getPopularity() * 5) / 100);
        LayerDrawable stars = (LayerDrawable) ratingBar1.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        System.out.println("==rating" + data.getPopularity());
        if (connectionDetector.isConnectingToInternet()) {
            final CustomProgressDialog pd = new CustomProgressDialog(context);
            pd.setTitle("Loading...");
            pd.show();
            app.getApiRequestHelper().movieDetail(id, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    pd.dismiss();
                    final BackdropData backdropData = (BackdropData) object;
                    posters = backdropData.getBackdrops();
                    slider_image_list = new ArrayList<>();
                    file_maps = new HashMap<>();
                    if (posters.size() < 1) {
                        file_maps.put("aa", "http://image.tmdb.org/t/p/w185" + data.getPosterPath());
                        tvOverview.setText(overview);
                        tvTitle.setText(title);
                        ratingBar1.setRating(d);
                        imageSlider(file_maps);
                    } else {
                        for (int i = 0; i < posters.size(); i++) {
                            if (file_maps.size() < 5) {
                                file_maps.put(i + "", "http://image.tmdb.org/t/p/w185" + posters.get(i).getFilePath());
                                System.out.println("==in if" + "http://image.tmdb.org/t/p/w185" + posters.get(i).getFilePath());
                                System.out.println("==size" + file_maps.size());
                            }
                        }
                        imageSlider(file_maps);
                        ratingBar1.setRating(d);
                        tvOverview.setText(overview);
                        tvTitle.setText(title);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    Log.d("==DetailsException", "" + apiResponse);
                    pd.dismiss();
                }
            });

        } else {
            Utils.showShortToast(context, "Please Turn On Internet Connection");
        }
    }

    private void imageSlider(HashMap<String, String> imagepath) {
        for (String name : imagepath.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
//                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);
            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Foreground2Background);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Utils.hideSoftKeyboard(DetailActivity.this);
                finish();
                overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
