package com.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.myapplication.DetailActivity;
import com.myapplication.R;
import com.myapplication.model.Result;
import com.myapplication.widget.CustomTextViewRegular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Admin on 4/26/2017.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.RecyclerViewHolder> {
    Context context;
    List<Result> shopList = new ArrayList<>();


    public MovieAdapter(Context context, List<Result>  shopList) {
        this.context = context;
        this.shopList = shopList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_listing, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        System.out.println("poster_path" + "On Bind");
        final Result shopData = shopList.get(position);
        String base = shopData.getPosterPath();
        System.out.println("poster_path" + shopData.getTitle());
        holder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                context.startActivity(new Intent(context, DetailActivity.class).putExtra("id",shopData.getId()));
                context.startActivity(new Intent(context, DetailActivity.class).putExtra("id", shopData));
            }
        });
        holder.tvShopNAme.setText(shopData.getTitle());
        holder.tvDate.setText(shopData.getReleaseDate());
        Picasso.with(context)
                .load("http://image.tmdb.org/t/p/w185/" + base)
                .resize(50, 50)
                .centerCrop()
                .into(holder.ivLiqrImage);
    }

    @Override
    public int getItemCount() {
        return shopList.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_liquor_image)
        ImageView ivLiqrImage;
        @BindView(R.id.tv_title)
        CustomTextViewRegular tvShopNAme;
        View itemView;
        @BindView(R.id.tv_date)
        CustomTextViewRegular tvDate;
        RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
        }

        public View getView() {
            return itemView;
        }
    }

}


