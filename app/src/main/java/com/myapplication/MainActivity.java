package com.myapplication;


import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.icu.text.IDNA;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.myapplication.adapter.MovieAdapter;
import com.myapplication.api.ApiRequestHelper;
import com.myapplication.model.Example;
import com.myapplication.model.Result;
import com.myapplication.util.ConnectionDetector;
import com.myapplication.util.ItemOffsetDecoration;
import com.myapplication.util.Utils;
import com.myapplication.widget.CustomTextViewRegular;
import com.myapplication.widget.materialprogress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    Context context;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    ConnectionDetector connectionDetector;
    MyApp app;
    private LinearLayoutManager layoutManager;
    MovieAdapter shopAdapter;
    List<Result> shopList = new ArrayList<>();
    List<Result> results = new ArrayList<>();
    @BindView(R.id.linear_map)
    LinearLayout linearMap;
    boolean loading = false;
    int initialstart = 1;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular textViewTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        app = (MyApp) getApplication();
        context = MainActivity.this;
        init();
        loadMovieData();
    }

    private void init() {
        connectionDetector = new ConnectionDetector(context);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen.dimen_5);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(itemDecoration);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        textViewTitle.setText("Assignment");
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.icon_dots);
        toolbar.setOverflowIcon(drawable);
    }

    private void loadMovieData() {
        String api_key = "b7cd3340a794e5a2f35e3abb820b497f";
        Map<String, String> params = new HashMap<>();
        params.put("api_key", api_key);
        if (connectionDetector.isConnectingToInternet()) {
            final CustomProgressDialog pd = new CustomProgressDialog(context);
            pd.setTitle("Loading...");
            pd.show();
            app.getApiRequestHelper().add_shop(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    pd.dismiss();
                    final Example shopResponce = (Example) object;

                    if (shopResponce.getResults() != null) {
                        Log.d("=====object", "" + object);
                        Log.d("=====Responce", "" + object);
                        shopList = shopResponce.getResults();
                        for (Result item : shopList) {
                            System.out.println(item);
                            results.add(item);
                        }
                        shopAdapter = new MovieAdapter(context, results);
                        recyclerView.setAdapter(shopAdapter);
                        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                            }

                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                switch (newState) {
                                    case RecyclerView.SCROLL_STATE_DRAGGING:
                                        int totalItemCount = layoutManager.getItemCount();
                                        int lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                                        System.out.println("====TotalItemCount" + totalItemCount);
                                        System.out.println("====lastVisibleItem" + lastVisibleItem);

                                        if (totalItemCount > 1) {
                                            if (lastVisibleItem >= totalItemCount - 1) {
                                                if (!loading) {
                                                    initialstart = initialstart + 1;
                                                    String startvalue = String.valueOf(initialstart);
                                                    loadMoreMovieData(startvalue);
                                                }
                                            }
                                        }
                                }
                            }
                        });

                    } else {
                        loading = true;
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    Log.d("==ShopsException", "" + apiResponse);
                    pd.dismiss();
                }
            });


        } else {
            Utils.showShortToast(context, "Please Turn On Internet Connection");
        }

    }

    private void loadMoreMovieData(String pageno) {
        String api_key = "b7cd3340a794e5a2f35e3abb820b497f";
        Map<String, String> params = new HashMap<>();
        params.put("api_key", api_key);
        params.put("page", pageno);
        if (connectionDetector.isConnectingToInternet()) {
            final CustomProgressDialog pd = new CustomProgressDialog(context);
            pd.setTitle("Loading...");
            pd.show();
            app.getApiRequestHelper().add_shop(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    pd.dismiss();
                    final Example shopResponce = (Example) object;
                    if (shopResponce.getResults() != null) {
                        Log.d("=====object", "" + object);
                        Log.d("=====Responce", "" + object);
                        if (shopResponce.getPage() == 11) {
                            loading = true;
                        } else {
                            shopList = shopResponce.getResults();
                            for (Result item : shopList) {
                                System.out.println(item);
                                results.add(item);
                            }
                            shopAdapter = new MovieAdapter(context, results);
                            recyclerView.setAdapter(shopAdapter);
                        }
                    } else {
                        loading = true;
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    Log.d("==ShopsException", "" + apiResponse);
                    pd.dismiss();
                }
            });

        } else {
            Utils.showShortToast(context, "Please Turn On Internet Connection");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info:
                startActivity(new Intent(MainActivity.this, InfoActivity.class));
                break;
        }
        return true;
    }
}
