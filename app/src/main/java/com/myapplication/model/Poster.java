package com.myapplication.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Poster implements Serializable
{

@SerializedName("aspect_ratio")
@Expose
private float aspectRatio;
@SerializedName("file_path")
@Expose
private String filePath;
@SerializedName("height")
@Expose
private int height;
@SerializedName("iso_639_1")
@Expose
private String iso6391;
@SerializedName("vote_average")
@Expose
private float voteAverage;
@SerializedName("vote_count")
@Expose
private int voteCount;
@SerializedName("width")
@Expose
private int width;
private final static long serialVersionUID = 3240186093005581867L;

public float getAspectRatio() {
return aspectRatio;
}

public void setAspectRatio(float aspectRatio) {
this.aspectRatio = aspectRatio;
}

public String getFilePath() {
return filePath;
}

public void setFilePath(String filePath) {
this.filePath = filePath;
}

public int getHeight() {
return height;
}

public void setHeight(int height) {
this.height = height;
}

public String getIso6391() {
return iso6391;
}

public void setIso6391(String iso6391) {
this.iso6391 = iso6391;
}

public float getVoteAverage() {
return voteAverage;
}

public void setVoteAverage(int voteAverage) {
this.voteAverage = voteAverage;
}

public int getVoteCount() {
return voteCount;
}

public void setVoteCount(int voteCount) {
this.voteCount = voteCount;
}

public int getWidth() {
return width;
}

public void setWidth(int width) {
this.width = width;
}

}